#include <stdio.h>

#include "bdOS.h"


extern TCBList *ready_queue;
extern TCBList *waiting_queue;

void p4_fn(void *thread_arg __attribute__((unused))){
  bdOS_exit(4);
}

void p3_fn(void *thread_arg __attribute__((unused))){
  uint8_t t4 = bdOS_spawn(p4_fn, 0, 0);
  for (int i = 20; i < 30; i++){
      cli();
      printf("p3 : %d\n", i);
      sei();
      _delay_ms(500);
  }
  uint8_t t4_ret = bdOS_join(t4);
  printf("[+] p4 done : %d\n", t4_ret);
  bdOS_exit(3);
}

void p2_fn(void *thread_arg __attribute__((unused))){
  uint8_t t3 = bdOS_spawn(p3_fn, 0, 0);
  uint8_t t3_ret = bdOS_join(t3);
  printf("[+] p3 done : %d\n", t3_ret);
  for (int i = 10; i < 20; i++){
      cli();
      printf("p2 : %d\n", i);
      sei();
      _delay_ms(500);
  }
  bdOS_exit(2);
}

void p1_fn(void *thread_arg __attribute__((unused))){
  uint8_t t2 = bdOS_spawn(p2_fn, 0, 0);
  uint8_t t2_ret = bdOS_join(t2);
  printf("[+] p2 done : %d\n", t2_ret);
  
  for (int i = 0; i < 10; i++){
      cli();
      printf("p1 : %d\n", i);
      sei();
      _delay_ms(500);
  }
  bdOS_exit(1);
}

void init_run(void){ 
  uint8_t t1 = bdOS_spawn(p1_fn, 0, 3);
  bdOS_detach(t1);
  uint8_t t1_ret = bdOS_join(t1);
  printf("[?] p1 done : %d\n", t1_ret);
}

int main(void){
  bdOS_start(init_run);
}
