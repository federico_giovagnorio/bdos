# set this to false to disable sonars in firmware
CC=avr-gcc
AS=avr-gcc
INCLUDE_DIRS=-I.
CC_OPTS=-Wall --std=gnu99 -DF_CPU=16000000UL -O3 -funsigned-char -funsigned-bitfields  -fshort-enums -Wall -Wstrict-prototypes -mmcu=atmega2560 $(INCLUDE_DIRS)  -D__AVR_3_BYTE_PC__
AS_OPTS=-x assembler-with-cpp $(CC_OPTS)

AVRDUDE=avrdude

# com1 = serial port. Use lpt1 to connect to parallel port.
AVRDUDE_PORT = /dev/ttyACM0    # programmer connected to serial device

AVRDUDE_WRITE_FLASH = -U flash:w:$(TARGET):i
AVRDUDE_FLAGS = -p m2560 -P $(AVRDUDE_PORT) -c $(AVRDUDE_PROGRAMMER) -b 115200
AVRDUDE_FLAGS += $(AVRDUDE_NO_VERIFY)
AVRDUDE_FLAGS += $(AVRDUDE_VERBOSE)
AVRDUDE_FLAGS += $(AVRDUDE_ERASE_COUNTER)
AVRDUDE_FLAGS += -D -q -V -C /usr/share/arduino/hardware/tools/avr/../avrdude.conf
AVRDUDE_FLAGS += -c wiring


OBJS=atomport_asm.o\
     bdOS_uart.o\
     bdOS_tcb.o\
     bdOS_tcb_list.o\
     bdOS_timer.o\
     bdOS_scheduler.o\
     bdOS_pool_allocator.o\
     bdOS_context.o\
     bdOS.o\
     bdOS_preempt.o\
	 bdOS_exit.o\
	 bdOS_spawn.o\
	 bdOS_join.o\
	 bdOS_detach.o\
	 bdOS_sleep.o\
	 bdOS_write.o\
	 bdOS_read.o\
	 bdOS_zombie.o\
	 

HEADERS=atomport_asm.h\
		bdOS_uart.h\
        bdOS_tcb.h\
		bdOS_bdOS_tcb_list.h\
		bdOS_timer.h\
        bdOS_scheduler.h\
		bdOS.h\
		bdOS_constants.h\
                bdOS_context.h\
		bdOS_globals.h\
		bdOS_syscall.h\
		bdOS_pool_allocator.h\
		
BINS= test_1.elf\
      test_2.elf\
      test_3.elf\
      test_4.elf\
      test_5.elf\
	  
.phony:	clean all

all:	$(BINS) 

#common objects
%.o:	%.c 
	$(CC) $(CC_OPTS) -c  $<

%.o:	%.s 
	$(AS) $(AS_OPTS) -c  $<

%.elf:	%.o $(OBJS)
	$(CC) $(CC_OPTS) -o $@ $< $(OBJS) $(LIBS)


%.hex:	%.elf
	avr-objcopy -O ihex -R .eeprom $< $@
	$(AVRDUDE) $(AVRDUDE_FLAGS) -U flash:w:$@:i #$(AVRDUDE_WRITE_EEPROM) 

clean:	
	rm -rf $(OBJS) $(BINS) *.hex *~ *.o
