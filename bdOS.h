#include "bdOS_tcb.h"

typedef void(*SyscallFunctionType)(void);

void bdOS_start(void (*init_run)(void));

void bdOS_preempt(void);
void bdOS_exit(uint8_t exit_val); 
uint8_t bdOS_spawn(ThreadFn function, void *args, uint16_t priority);
uint8_t bdOS_join(uint8_t tid);
uint8_t bdOS_detach(uint8_t tid);
uint16_t bdOS_sleep(uint16_t time_ms);
uint8_t bdOS_write(char *buff, uint8_t count);
uint8_t bdOS_read(char *buff, uint8_t count);

