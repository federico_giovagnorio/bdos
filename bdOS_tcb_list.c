#include "bdOS_inclib.h"

#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"

extern TCB *waiting_queue;

/* PRELEVA TCB IN TESTA DA UNA CODA (ready o waiting) */
TCB* TCBList_dequeue(TCBList* list){
  TCB* tcb=list->first;
  if (tcb==NULL)
    return tcb;
  
  if (list->size==1) {
    list->first = list->last = NULL;
  } else {
    TCB* next=tcb->next;
    list->first=next;
    next->prev=next;
  }
  --list->size;
  tcb->next=NULL;
  tcb->prev=NULL;
  return tcb;
}

/* INSERISCE TCB ALLA FINE DI UNA CODA (ready o waiting) */
uint8_t TCBList_enqueue(TCBList* list, TCB* tcb){
  if(tcb->prev!=NULL || tcb->next!=NULL)
    return ERROR;
  
  if (!list->size) {
    list->first=tcb;
    list->last=tcb;
    tcb->prev=tcb;
    tcb->next=NULL;
  } else {
    list->last->next=tcb;
    tcb->prev=list->last;
    tcb->next=NULL;
    list->last=tcb;
  }
  ++list->size;
  return OK;
}

/* RIMUOVE TCB GENERICO DA UNA CODA E LO RITORNA (ready o waiting) */
TCB *TCBList_detach(TCBList* list, TCB* e){
  TCB* tcb=list->first;
  if (tcb==NULL)
    return NULL;
  TCB* prev;
  TCB* next;
  while (tcb){
    prev = tcb->prev;
    next = tcb->next;
    if (tcb == e){
      if (prev == tcb && next == NULL){
        list->first = NULL;
        list->last = NULL;
        break;
      } 
      else if (prev == tcb){
        list->first = next;
        list->first->prev = list->first;
        break;
      }
      else if (next == NULL) {
        list->last = prev;
        prev->next = NULL;
        break;
      }
      else {             
        prev->next = next;
        next->prev = prev;
        break;
      }
    }
    tcb = tcb->next;
  }
  if (tcb) {
    --list->size;
    if (list->size == 1){
      list->first->prev = list->first;
      list->first->next = NULL;
    }
    tcb->next=NULL;
    tcb->prev=NULL;
  }
  return tcb;
}

/* RIMUOVE FIGLIO CON TID PARI A TID tid DA UNA COPDA DI FIGLI E LO RITORNA (children) */ 
TCB *TCBList_remove_child(TCBList* list, uint8_t tid){
  if (list == NULL || list->first == NULL)
    return NULL;
  TCB *prev = list->first;
  TCB *aux = list->first;
  if (aux->tid == tid){
    list->first = aux->next;
    list->size --;
    return aux;
  }
  while (aux){
    if (aux->tid == tid){
        prev->brother = aux->brother;
        list->size --;
        return aux;
    }
    prev = aux;
    aux = aux->brother;
  } 
  return NULL;
}

/* AGGIUNGE TCB IN TESTA AD UNA CODA DI FIGLI (children)*/ 
void TCBList_add_child(TCBList* list, TCB *tcb){
  if (list == NULL)
    return;
  TCB* aux = list->first;
  list->first = tcb;
  tcb->brother = aux; 
  list->size++;   
}

/* STAMPA UNA CODA DI TCB (ready o waiting) */
void TCBList_print(TCBList* list){
  TCB* aux=list->first;
  printf("[");
  while(aux!=NULL){
    printf(" %d ", aux->tid);
    aux=aux->next;
  }
  printf("]\n");
}

/* STAMPA UNA CODA DI TCB (children) */
void TCBList_print_child(TCBList* list){
  TCB* aux=list->first;
  printf("[");
  while(aux!=NULL){
    printf(" %d ", aux->tid);
    aux=aux->brother;
  }
  printf("]\n");
}
