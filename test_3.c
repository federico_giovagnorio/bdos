#include <stdio.h>

#include "bdOS.h"

void p1_fn(void *thread_arg __attribute__((unused))){
  for (int i = 0; i < 10; i++){
      cli();
      printf("p1 : %d\n", i);
      sei();
      bdOS_sleep(100);
  }
  bdOS_exit(1);
}

void p2_fn(void *thread_arg __attribute__((unused))){
  for (int i = 10; i < 20; i++){
    cli();
    printf("p2 : %d\n", i);
    sei();
    bdOS_sleep(500);
  }
  bdOS_exit(2);
}

void p3_fn(void *thread_arg __attribute__((unused))){
  for (int i = 20; i < 30; i++){
      cli();
      printf("p3 : %d\n", i);
      sei();
      bdOS_sleep(1000);
  }
  bdOS_exit(3);
}

void init_run(void){
  uint16_t prio_p1 = 0;
  uint16_t prio_p2 = 0;
  uint16_t prio_p3 = 0;
  
  bdOS_spawn(p1_fn, 0, prio_p1);
  bdOS_spawn(p2_fn, 0, prio_p2);
  bdOS_spawn(p3_fn, 0, prio_p3);
}


int main(void){
  bdOS_start(init_run);
}
