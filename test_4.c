#include "stdio.h"

#include "bdOS.h"

void p1_fn(void *thread_arg __attribute__((unused))){
  char p1_buf[] = "I'm p1\n"; 
  for (int i = 0; i < 10; i++){
      bdOS_write(p1_buf, sizeof(p1_buf));
      bdOS_sleep(500);
  }
  bdOS_exit(1);
}

void p2_fn(void *thread_arg __attribute__((unused))){
  // CERCA DI SCRIVERE PIU BYTE DI QUANTO E' GRANDE IL TX_BUFFER
  char p2_buf[129];
  bdOS_write(p2_buf, sizeof(p2_buf));
  bdOS_exit(2);
}

void p3_fn(void *thread_arg __attribute__((unused))){
  char p3_buf[5];

  bdOS_sleep(7000);

  uint8_t p3_read_bytes = bdOS_read(p3_buf, 5);
  bdOS_write(p3_buf, p3_read_bytes);

  bdOS_sleep(1000);
  
  p3_read_bytes = bdOS_read(p3_buf, 5);
  bdOS_write(p3_buf, p3_read_bytes);
  
  bdOS_exit(3);
}


void init_run(void){
  uint16_t prio_p1 = 0;
  uint16_t prio_p2 = 0;
  uint16_t prio_p3 = 0;

  bdOS_spawn(p1_fn, 0, prio_p1);
  bdOS_spawn(p2_fn, 0, prio_p2);
  bdOS_spawn(p3_fn, 0, prio_p3);
  
}


int main(void){
  bdOS_start(init_run);
}