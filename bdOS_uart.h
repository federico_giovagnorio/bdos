#pragma once

#define UART_BUFFER_SIZE 128

typedef struct {
    int buffer[UART_BUFFER_SIZE];
    volatile uint8_t start;
    volatile uint8_t end;
    volatile uint8_t size;
}UART_buffer;

void printf_init(void);
void usart_putchar(char);
char usart_getchar(void);
