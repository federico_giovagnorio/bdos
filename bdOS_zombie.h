#pragma once 

/* STRUCT Zombie */
typedef struct Zombie {
    struct Zombie *next;
    uint8_t tid;
    uint8_t exit_value;
} Zombie;

/* STRUCT ZombieList */
typedef struct ZombieList {
    Zombie *first;
    Zombie *last;
    uint8_t size;
} ZombieList;

PoolAllocatorResult Zombie_init(void);
struct Zombie* Zombie_alloc(void);
PoolAllocatorResult Zombie_dealloc(struct Zombie *);
struct Zombie *ZombieList_remove_Zombie(ZombieList *, uint8_t);
void ZombieList_add_child(ZombieList* list, struct Zombie *);
void ZombieList_print(ZombieList *);
