#include "bdOS_inclib.h"

#include "atomport_asm.h"

#include "bdOS.h"
#include "bdOS_scheduler.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"
#include "bdOS_zombie.h"

/* **************************** *
 *            EXIT              *
 * **************************** */

extern TCB *running;
extern TCBList *ready_queue;
extern TCBList *waiting_queue;
extern PoolAllocator tcb_allocator;
extern uint8_t need_schedule;


/* UN THREAD PUO EFFETTUARE JOIN SOLTANTO SU UN SUO FIGLIO, ALLORA TUTTI I FIGLI
 * DEL THREAD USCENTE NON POTRANNO PIU ESSERE JOINATI, POICHE IL LORO  PARENT DIVENTA NULL
 * TUTTI I SUOI ZOMBIE VENGONO DEALLOCATI, POICHE NESSUN ALTRO THREAD POTRA RECUPERARLI
 * SE IL PARENT DEL RUNNING ERA IN ATTESA DELLA SUA TERMINAZIONE, ALLORA QUESTO VIENE SBLOCCATO 
 * SE È JOINABLE, ALLORA VIENE INSERITO UN NUOVO ZOMBIE NELLA LISTA ZOMBIE DEL PADRE
 * INFINE VIENE DEALLOCATO IL SUO TCB E RELATIVO STACK */
void internal_exit(void){
    running->status = Terminated;
    running->syscall_retvalue = running->syscall_args[0];

    //detach di tutti i figli
    TCB *child = running->tcb_children.first;
    while (child) {
        child->parent = NULL;
        child = child->brother;
    }
    
    //deallocazione dei suoi zombie
    Zombie *zombie = running->zombie_children.first;
    Zombie *dealloc_zombie;
    while (zombie) {
        dealloc_zombie = zombie;
        zombie = zombie->next;
        Zombie_dealloc(dealloc_zombie);
    }

    //controllo se il thread uscente ha un padre
    if (running->parent != NULL && running->mode == Joinable){
        //se padre in attesa, allora il padre viene reinserito nella coda di ready, e lo zombie uscente viene consumato automaticamente
        if (running->parent->status == Waiting && (uint16_t)running->parent->syscall_args[0] == running->tid){
            TCB *detached = TCBList_detach(waiting_queue, running->parent);
            
            if (!detached)
                printf ("[-] No detached thread (%d) from waiting [internal_exit()]\n", running->parent->tid);
            else {
                //detached->syscall_retvalue = running->syscall_retvalue;
                detached->status = Ready;
                TCBList_enqueue(ready_queue, detached);
            }
        }
        //se padre non in attesa, allora viene inserito uno zombie relativo al thread uscente nella lista del padre
        else {
            TCBList_remove_child(&(running->parent->tcb_children), running->tid);
            Zombie *new_zombie = Zombie_alloc();
            if (new_zombie){
                new_zombie->tid = running->tid;
                new_zombie->exit_value = running->syscall_retvalue;
                ZombieList_add_child(&running->parent->zombie_children, new_zombie);
            }
            else     
                printf ("[-] No zombie created for exiting thread [internal_exit()]\n");
            
        }
    }

    //deallocazione del tcb, si richiede scheduling
    TCB_dealloc(running);
    need_schedule = 1;
}
