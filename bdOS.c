#include "bdOS_inclib.h"

#include "atomport_asm.h"

#include "bdOS.h"
#include "bdOS_constants.h"
#include "bdOS_context.h"
#include "bdOS_scheduler.h"
#include "bdOS_syscall.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"
#include "bdOS_uart.h"
#include "bdOS_zombie.h"

/* **************************** *
 *     VARIABILI GLOBALI        *  
 * **************************** */ 

/* THREAD IN RUNNING E TID INCREMENTALE */
TCB *running;
uint8_t next_tid;


/* FLAG PER SYSCALL CHE RICHIEDONO UNA SCHEDULE UNA VOLTA TERMINATE */
uint8_t need_schedule;


/* CODE DI TCB READY E WAITING */
TCBList ready_q = {
  .first = NULL,
  .last = NULL,
  .size = 0
};

TCBList waiting_q = {
  .first = NULL,
  .last = NULL,
  .size = 0
};

TCBList *ready_queue = &ready_q;
TCBList *waiting_queue = &waiting_q;


/* VETTORE FUNZIONI E ARGOMENTI SYSCALL */
SyscallFunctionType syscall_vector[BDOS_MAX_SYSCALLS];
uint8_t syscall_numarg[BDOS_MAX_SYSCALLS];


/* CONTESTO PER TRAP E SCHEDULE */
Context internal_context;


/* ALLOCATORI E MEMORIA PER TCB E RELATIVI STACK E PER GLI ZOMBIE */
PoolAllocator tcb_allocator;
PoolAllocator zombie_allocator; 
char tcb_memory_block[TCB_MEMORY_BLOCK_SIZE];
char zombie_memory_block[ZOMBIE_MEMORY_BLOCK_SIZE];



/* **************************** *
 *   INIZIALIZZAZIONE SISTEMA   *
 * **************************** */

/* FUNZIONE ESEGUITA DA INIT. ESEGUE FUNZIONE PASSATA ALLA bsOS_start() E POI FA PREEMT ALL' INFINITO */
void init_fn(void *fn) {
  
  void(*run)(void) = (void(*)(void))fn;
  (*run)();
  
  while(1)
    bdOS_preempt();

}


/* INIZIALIZZA VARIABILI GLOBALI, INIZIALIZZA ALLOCATORI E CREA INIT CHE ESEGUIRA' LA FUNZIONE PASSATA COME ARGOMENTO */
void bdOS_start(void (*fn)(void)) {

  next_tid = 0;
  need_schedule = 0;

  
  //popola vettore syscall
  for (uint8_t i=0; i<BDOS_MAX_SYSCALLS; ++i){
    syscall_vector[i]=0;
  }
  
  syscall_vector[BDOS_CALL_PREEMPT]   = internal_preempt;
  syscall_numarg[BDOS_CALL_PREEMPT]   = 0;

  syscall_vector[BDOS_CALL_EXIT]   = internal_exit;
  syscall_numarg[BDOS_CALL_EXIT]   = 1;

  syscall_vector[BDOS_CALL_SPAWN]   = internal_spawn;
  syscall_numarg[BDOS_CALL_SPAWN]   = 3;

  syscall_vector[BDOS_CALL_JOIN]   = internal_join;
  syscall_numarg[BDOS_CALL_JOIN]   = 1;

  syscall_vector[BDOS_CALL_DETACH]   = internal_detach;
  syscall_numarg[BDOS_CALL_DETACH]   = 1;

  syscall_vector[BDOS_CALL_SLEEP]   = internal_sleep;
  syscall_numarg[BDOS_CALL_SLEEP]   = 1;

  syscall_vector[BDOS_CALL_WRITE]   = internal_write;
  syscall_numarg[BDOS_CALL_WRITE]   = 2;

  syscall_vector[BDOS_CALL_READ]   = internal_read;
  syscall_numarg[BDOS_CALL_READ]   = 2;

  
  //inizializzazione printf e quindi uart
  printf_init();

  
  //inizializzazione allocatori
  PoolAllocatorResult ret = TCB_init();
  if (ret){
    printf("[-] TCB Allocator error : %s [bdOS_start()]\n", PoolAllocator_strerror(ret));
    return;
  }
  
  ret = Zombie_init();
  if (ret){
    printf("[-] Zombie Allocator error : %s [bdOS_start()]\n", PoolAllocator_strerror(ret));
    return;
  }

  
  //creazione tcb init ed inserimento nella coda ready.
  TCB *init_tcb = TCB_alloc();
  if (init_tcb == NULL){
    printf("[-] Can' t create init thread [bdOS_start()]\n");
    return;
  }
  TCB_create(init_tcb, (Pointer)(((char *)init_tcb) + TCB_SIZE + TCB_STACK_SIZE - 1), init_fn, fn, TCB_DEFAULT_PRIORITY, next_tid);
  TCBList_enqueue(ready_queue, init_tcb);

  
  //inizia la schedule
  startSchedule();

}



/* **************************** *
 *       TRAP E SYSCALL         *
 * **************************** */

/* TRAP ESEGUITA NEL CONTESTO INTERNO */
void bdOS_trap(void) {
  
  int syscall_num = running->syscall_num;
  (*syscall_vector[syscall_num])();
  
  //context switch solo per non riabilitare ancora le interrupt, torniamo in bdOS_syscall().
  archContextSwitch((TCB *)&internal_context, running); 

}


/* ESECUZIONE SYSCALL SENZA INTERRUPT */
uint8_t bdOS_syscall(int syscall_num, ...) {
  
  //disattivazione interrupt
  cli();
  
  
  //lettura/scrittura argomenti e numero syscall
  va_list ap;
  if (syscall_num<0 || syscall_num > BDOS_MAX_SYSCALLS || syscall_vector[syscall_num] == NULL)
    return BDOS_ERR_SYSCALL;

  uint8_t nargs=syscall_numarg[syscall_num];
  va_start(ap,syscall_num);
  for (uint8_t i=0; i<nargs; ++i){
    running->syscall_args[i] = (void *) va_arg(ap,int);
  }
  va_end(ap);
  
  running->syscall_num=syscall_num;
  
  
  //esecuzione trap nel contesto interno
  bdOS_create_context(&internal_context, bdOS_trap);
  archContextSwitch(running, (TCB *)&internal_context);

  
  //controlla se la syscall in questione richiede schedule al termine (entry point al ritorno dalla trap) 
  if (need_schedule){
    need_schedule = 0;
    call_schedule();
  }
  
  
  //riabilitazione interrupt e ritorno
  sei();
  return running->syscall_retvalue;

}



/* **************************** *
 *     INTERFACCIA SYSCALL      *
 * **************************** */

/* RILASCIA VOLONTARIAMENTE LA CPU */
void bdOS_preempt() {
  internal_preempt();
}

/* TERMINA THREAD */
void bdOS_exit(uint8_t ret) {
  bdOS_syscall(BDOS_CALL_EXIT, ret);
}

/* CREA NUOVO THREAD. RITORNA TID DEL NUOVO THREAD CREATO SE SUCCESSO, ALTRIMENTI UN VALORE NAGATIVO */
uint8_t bdOS_spawn(ThreadFn fn, void *args, uint16_t priority) {
  return bdOS_syscall(BDOS_CALL_SPAWN, fn, args, priority);
}

/* ASPETTA THREAD FIGLIO CON TID tid. RITORNA EXIT VALUE DEL THREAD JOINATO SE SUCCESSO, ALTRIMENTI UN VALORE NEGATIVO */ 
uint8_t bdOS_join(uint8_t tid) {
  return bdOS_syscall(BDOS_CALL_JOIN, tid);
}

/* DETACH THREAD FIGLIO CON TID tid. RITORNA TID DEL THREAD DETACCHATO SE SUCCESSO, ALTRIMENTI UN VALORE NEGATIVO */
uint8_t bdOS_detach(uint8_t tid) {
  return bdOS_syscall(BDOS_CALL_DETACH, tid);
}

/* THREAD CHIAMANTE ATTENDE PER time MILLISECONDI. RITORNA MILLISECONDI ATTESI SE SUCCESSO, ALTRIMENTI UN VALORE NEGATIVO */
uint16_t bdOS_sleep(uint16_t time) {
  return bdOS_syscall(BDOS_CALL_SLEEP, time);
}

/* SCRIVE SULLA SERIALE 0 count BYTE DA buff. RITORNA NUMERO BYTE SCRITTI SE SUCCESSO, ALTRIMENTI UN VALORE NEGATIVO */
uint8_t bdOS_write(char *buff, uint8_t count) {
  return bdOS_syscall(BDOS_CALL_WRITE, buff, count);
}

/* LEGGE AL PIU COUNT BYTE DALLA SERIALE. RITORNA NUMERO DI BYTE LETTI. */
uint8_t bdOS_read(char *buff, uint8_t count) {
  return bdOS_syscall(BDOS_CALL_READ, buff, count);
}

