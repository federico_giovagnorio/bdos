#pragma once

/* *************************** *
 *     COSTANTI SYSCALL        *
 * *************************** */

#define BDOS_MAX_SYSCALLS      32
#define BDOS_MAX_SYSCALLS_ARGS  8

/* ID SYSTEM CALL */ 
#define BDOS_CALL_PREEMPT       1
#define BDOS_CALL_EXIT          2
#define BDOS_CALL_SPAWN         3
#define BDOS_CALL_JOIN          4
#define BDOS_CALL_DETACH        5
#define BDOS_CALL_SLEEP         6
#define BDOS_CALL_WRITE         7
#define BDOS_CALL_READ          8


/* ERRORE GENERICO SYSTEM CALL */
#define BDOS_ERR_SYSCALL       -1

/* ERRORE SPECIFICO SYSTEM CALL */
#define BDOS_ERR_EXIT          -2
#define BDOS_ERR_SPAWN         -3
#define BDOS_ERR_JOIN          -4
#define BDOS_ERR_DETACH        -5
#define BDOS_ERR_SLEEP         -6
#define BDOS_ERR_WRITE         -7


/* *************************** *
 *     COSTANTI TCB            *
 * *************************** */

/* DIMENSIONE BLOCCO MEMORIA PER I TCB */
#define TCB_MEMORY_BLOCK_SIZE 2048

/* DIMENSIONI TCB e STACK */
#define TCB_SIZE sizeof(TCB)
#define TCB_STACK_SIZE        256

/* MASSIMO NUMERO DI THREAD */
#define MAX_NUM_TCB TCB_MEMORY_BLOCK_SIZE/(TCB_SIZE + TCB_STACK_SIZE + sizeof(int))

/* PRIORITA' DEFAULT PER THREAD */
#define TCB_DEFAULT_PRIORITY 1

/* *************************** *
 *     COSTANTI CONTEXT        *
 * *************************** */

#define CONTEXT_STACK_SIZE      256

/* *************************** *
 *     COSTANTI ZOMBIE         *
 * *************************** */

/* DIMENSIONI BLOCCO MEMORIA PER ZOMBIE */
#define ZOMBIE_MEMORY_BLOCK_SIZE 256

/* DIMENSIONI DI UNO ZOMBIE */
#define ZOMBIE_SIZE sizeof(Zombie)

/* MASSIMO NUMERO DI ZOMBIE */
#define MAX_NUM_ZOMBIE (ZOMBIE_MEMORY_BLOCK_SIZE / (ZOMBIE_SIZE + sizeof(int)))
