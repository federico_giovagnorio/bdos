#include "bdOS.h"

/* **************************** *
 *           PREEMPT            *
 * **************************** */

extern uint8_t need_schedule;

/* SI RICHIEDE CHE AL RITORNO DELLA TRAP VENGA ESEGUITA UNA SCHEDULE */
void internal_preempt(void){
    need_schedule = 1;
}
