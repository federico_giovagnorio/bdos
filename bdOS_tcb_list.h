#pragma once 

#include "bdOS_tcb.h"

/* STRUTTURA TCBList */
typedef struct TCBList{
  struct TCB* first;
  struct TCB* last;
  uint8_t size;
} TCBList;

extern TCBList tcb_queue;
struct TCB* TCBList_dequeue(TCBList* list);
uint8_t TCBList_enqueue(TCBList* list,struct TCB* tcb);
void TCBList_print(TCBList* list);
struct TCB* TCBList_remove_child(TCBList *, uint8_t);
struct TCB *TCBList_detach(TCBList *, struct TCB *);
void TCBList_add_child(TCBList *,struct TCB *);
void TCBList_print_child(TCBList *);
