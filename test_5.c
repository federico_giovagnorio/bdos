#include <stdio.h>
#include <string.h>

#include "bdOS.h"

void p1_fn(void *thread_arg __attribute__((unused))){

 for (int i = 0; i < 10; i++){
      cli();
      printf("p1 : %d\n", i);
      sei();
      _delay_ms(500);
  }

  bdOS_exit(1);
}

void p2_fn(void *thread_arg __attribute__((unused))){

  char destroy[1024];
  memset(destroy, 0x00, 1024);
  printf("%s\n", destroy);
 
  for (int i = 10; i < 20; i++){
      cli();
      printf("p2 : %d\n", i);
      sei();
      _delay_ms(500);
  }
  bdOS_exit(2);
}

void init_run(void){
  uint16_t prio_p1 = 3;
  uint16_t prio_p2 = 0;
  
  bdOS_spawn(p1_fn, 0, prio_p1);
  bdOS_spawn(p2_fn, 0, prio_p2);
}


int main(void){
  bdOS_start(init_run);
}
