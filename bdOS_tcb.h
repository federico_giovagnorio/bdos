
#pragma once 
#include "bdOS_inclib.h"

#include "bdOS_constants.h"
#include "bdOS_tcb_list.h"
#include "bdOS_pool_allocator.h"
#include "bdOS_zombie.h"


#define OK     0
#define ERROR -1

typedef uint8_t* Pointer;
typedef void (* ThreadFn)(void *thread_args);

//thread status
typedef enum {Running=0x0, Terminated=0x1, Ready=0x2, Waiting=0x3} ThreadStatus;
//thread mode
typedef enum {Joinable=0x0, Detached=0x1} ThreadMode;

/* STRUTTURA TCB */
typedef struct TCB{
  Pointer sp_save_ptr;         /* Puntatore allo stack del thread (SP), cambia in ogni context_switch in cui è coinvolto il thread */
  
  ThreadFn thread_fn;          /* Funzione che il thread eseguirà */
  void *thread_arg;            /* Argomenti della funzione da eseguire */ 

  struct TCB* next;	       /* TCB precedente e successivo nella coda ready o waiing */
  struct TCB* prev;
  
  struct TCB* parent;          /* TCB genitore del thread */
  struct TCB* brother;         /* Per eventualmente essere inserito in una lista dei figli */
  

  ThreadStatus status;	       /* Ready, Waiting o Terminated */
  ThreadMode mode;             /* Joinable se padre puo effettuare join su di lui, Detached altrimenti */

  uint16_t priority;           /* La priority è una proprietà costante del TCB, */
  uint16_t curr_priority;      /* La curr_priority viene decrementata ad ogni ciclo di esecuzione del thread */

  TCBList tcb_children;        /* Lista dei thread figli del TCB */
  ZombieList zombie_children;  /* Lista dei zombie children direttamente in TCB, se padre termina, allora nessun altro è interessato al ritorno dei figli */

  uint8_t syscall_num;         /* Numero, valore di ritorno ed argomenti della syscall */
  uint8_t syscall_retvalue;
  void *syscall_args[BDOS_MAX_SYSCALLS_ARGS];
  
  uint8_t tid;		       /* Thread ID, unico per TCB */
  uint16_t timer;              /* Timer per eventuali sleep */


} TCB;


void TCB_create(TCB* tcb, Pointer stack_top, ThreadFn thread_fn, void *thread_arg, uint16_t priority, uint8_t tid);
PoolAllocatorResult TCB_init(void);
TCB* TCB_alloc(void);
PoolAllocatorResult TCB_dealloc(TCB *);
