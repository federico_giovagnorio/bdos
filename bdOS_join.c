#include "bdOS_constants.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"
#include "bdOS_scheduler.h"
#include "bdOS_zombie.h"

/* **************************** *
 *  	      JOIN              *
 * **************************** */

extern TCB *running;
extern TCBList *waiting_queue;
extern uint8_t need_schedule;

/* UN THREAD PUO ASPETTARE SOLO LA TERMINAZIONE DI UN SUO FIGLIO.
 * SE tid_to_wait È GIA TERMINATO, ALLORA È TRA I ZOMBIE DEL RUNNING, ED IL SUO ZOMBIE PUO ESSERE CONSUMATO
 * SE INVECE È TRA I SUOI FIGLI, ALLORA IL RUNNING VA MESSO IN WAITING E SI RICHIEDE LA SCHEDULE */ 
void internal_join(void) {
    
    uint16_t tid_to_wait = (uint16_t) running->syscall_args[0];
    //controllo per tid valido
    if (tid_to_wait < 0){
        running->syscall_retvalue = BDOS_ERR_JOIN;
    }
    

    //controllo tra i figli
    if (running->tcb_children.first != NULL) {
        TCB *aux_child = running->tcb_children.first;
        while (aux_child) {
	    //se tra i figli, allora il running va nella coda di waiting
            if (aux_child->tid == tid_to_wait) {
                running->status = Waiting;
                TCBList_enqueue(waiting_queue, running);
                need_schedule = 1;
                return;
            }
            aux_child = aux_child->brother;
        }
    }


    //controllo tra gli zombie
    Zombie *aux_zombie = ZombieList_remove_Zombie(&running->zombie_children, tid_to_wait);
    //se tra gli zombie, allora tale entry va rimossa, e si puo procedere senza necessita di schedule
    if (aux_zombie) {
        running->syscall_retvalue = aux_zombie->exit_value;
        Zombie_dealloc(aux_zombie);
        return;
    }
    
    
    //allora il tid richiesto non è ne tra i suoi figli ne tra i suoi zombie, si ritorna con un errore
    running->syscall_retvalue = BDOS_ERR_JOIN;
    return;

}
