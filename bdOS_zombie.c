#include "bdOS_inclib.h"

#include "bdOS_constants.h"
#include "bdOS_pool_allocator.h"
#include "bdOS_zombie.h"

/* **************************** *
 *   	    ZOMBIE              *
 * **************************** */

extern PoolAllocator zombie_allocator;
extern char zombie_memory_block[];

/* INIZIALIZZA ALLOCATORE PER ZOMBIE */
PoolAllocatorResult Zombie_init(){
  PoolAllocatorResult result=PoolAllocator_init(&zombie_allocator,
				  ZOMBIE_SIZE,
				  MAX_NUM_ZOMBIE ,
				  zombie_memory_block,
				  ZOMBIE_MEMORY_BLOCK_SIZE);
    return result;
}

/* ALLOCA UNO ZOMBIE TRAMITE L' ALLOCATORE */
Zombie* Zombie_alloc() {
  Zombie* zombie = (Zombie*) PoolAllocator_getBlock(&zombie_allocator);
  return zombie;
}

/* DEALLOCA UNO ZOMBIE TRAMITE L' ALLOCATORE */
PoolAllocatorResult Zombie_dealloc(Zombie *zombie) {
   return PoolAllocator_releaseBlock(&zombie_allocator, zombie);
}

/* RIMUOVE ZOMBIE PER TID DA UNA ZombieList */
Zombie *ZombieList_remove_Zombie(ZombieList* list, uint8_t tid){
  if (list == NULL || list->first == NULL)
    return NULL;
  Zombie *prev = list->first;
  Zombie *aux = list->first;
  if (aux->tid == tid){
    list->first = aux->next;
    list->size --;
    return aux;
  }
  while (aux){
    if (aux->tid == tid){
        prev->next = aux->next;
        list->size --;
        return aux;
    }
    prev = aux;
    aux = aux->next;
  } 
  return NULL;
}

/* AGGIUNGE UNO ZOMBIE IN TESTA AD UNA ZombieList */
void ZombieList_add_child(ZombieList* list, Zombie *zombie){
  if (list == NULL)
    return;
  Zombie* aux = list->first;
  list->first = zombie;
  zombie->next = aux; 
  list->size++;   
}

/* STAMPA UNA ZombieList */
void ZombieList_print(ZombieList* list){
  Zombie* aux=list->first;
  printf("[");
  while(aux!=NULL){
    printf(" (%d,%d) ", aux->tid, aux->exit_value);
    aux=aux->next;
  }
  printf("]\n");
}
