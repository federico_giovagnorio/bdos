#include "bdOS_inclib.h"

#include "atomport_asm.h"

#include "bdOS.h"
#include "bdOS_context.h"
#include "bdOS_scheduler.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"
#include "bdOS_timer.h"

/* **************************** *
 *         SCHEDULER            *
 * **************************** */

extern TCB *running;
extern TCBList *ready_queue;
extern TCBList *waiting_queue;
extern Context internal_context;


/* MODIFICA internal_context PER ESEGUIRE LA SCHEDULE E SI PASSA A QUEST' ULTIMO */
void call_schedule(void){
    bdOS_create_context(&internal_context, schedule);
    archContextSwitch(running, (TCB *)&internal_context);
}

/* PRELEVA INIT DALLA CODA DI READY E LO ESEGUE, ATTIVANDO TIMER ED INTERRUPT */
void startSchedule(void){
  
  running = TCBList_dequeue(ready_queue);
  if (running == NULL){
    printf("[-] No ready thread aviable [start_schedule()]\n");
    return;
  }
  running->status = Running;
  
  timerStart();
  archFirstThreadRestore(running);
}


/* ESEGUE SCHEDULING, EFFETTUANDO RESTORE SUL *NUOVO* RUNNING */
void schedule(void) {
  
  //aggiorna timer dei thread in sleeping tra quelli in waiting
  if (waiting_queue->first) {
    TCB *aux = waiting_queue->first;
    while (aux){
      if (aux->syscall_num == BDOS_CALL_SLEEP){
        aux->timer -= ((double)1)/SYSTEM_TICKS_PER_SEC * 1000;
        if ((int)aux->timer < 0){
          TCB *wake_up = TCBList_detach(waiting_queue, aux);
          if (wake_up){
            wake_up->status = Ready;
            wake_up->timer = 0;
            wake_up->syscall_retvalue = (uint16_t) wake_up->syscall_args[0];
            TCBList_enqueue(ready_queue, wake_up);
          }
          else
            printf("[-] No wake_up detached from waiting [schedule()]\n");
        }
      }
      aux = aux->next;
    }
  }

  //unico thread disponibile è l' auttuale running, allora torno
  if (ready_queue->first == NULL)
    archFirstThreadRestore(running);
    
  //se thread ha effettuato exit, join oopure sleep, non va reinserito nella coda ready
  if (running->status == Terminated || running->status == Waiting){
    running = TCBList_dequeue(ready_queue);
    running->status = Running;
    archFirstThreadRestore(running);
  }

  //se thread ha ancora priorita da spendere, allora il running rimane lo stesso
  running->curr_priority --;
  if (running->curr_priority > 0)
    archFirstThreadRestore(running);

  //allora il thread in esecuzione va cambiato, viene resettata priorita e si passa al nuovo thread
  running->curr_priority = running->priority;
  running->status = Ready;
  TCBList_enqueue(ready_queue, running);  

  running = TCBList_dequeue(ready_queue);
  running->status = Running;
  archFirstThreadRestore(running);
  
}
