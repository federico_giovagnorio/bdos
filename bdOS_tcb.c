#include "bdOS_inclib.h"

#include "bdOS.h"
#include "bdOS_scheduler.h"
#include "bdOS_syscall.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"

extern TCB *running;
extern uint8_t next_tid;
extern PoolAllocator tcb_allocator;
extern char tcb_memory_block[];

/* ESEGUE LA FUNZIONE SPECIFICA NELLA CREAZIONE DEL THREAD
 * _trampoline TERMINA SOLO SE THREAD TERMINA LA SUA FUNZIONE SENZA AVER ESEGUITO UNA EXIT
 * ALLORA VIENE INSERITA UNA EXIT PRECAUZIONALE, ESEGUITA SOLO IN QUESTO CASO
 * IN QUESTO MODO LIBERIAMO LA MEMORIA DEL THREAD QUANDO TERMINA E POSSIAMO ESEGUIRE UN ALTRO THREAD
 * ALTRIMENTI SI VA INCONTRO AD ERRORI DOVUTI ALLA DEALLOCAZIONE DEI RECORD DI ATTIVAZIONE
 * INFATTI SE TRAMPOLINE TERMINA, E SI CONTINUA SU QUESTA LINEA DI ESECUZIONE, IL COMPORTAMENTO È CASUALE */
static void _trampoline(void){  
  
  // per come è impostato ora lo scheduler, ho solo thread_restore, che attiva le interrupt al ritorno del context_switch (reti)
  //sei();
  
  if (running && running->thread_fn) {
    (*running->thread_fn)(running->thread_arg);
  }
  
  //exit precauzionale
  bdOS_exit(BDOS_ERR_EXIT);
}

/* INIZIALIZZA TCB E CREA IL SUO CONTESTO NELLO STACK */
void TCB_create(TCB* tcb, Pointer stack_top, ThreadFn thread_fn, void *thread_arg, uint16_t priority, uint8_t tid){
  
  //inizializzazione valori tcb
  tcb->thread_fn=thread_fn;
  tcb->thread_arg=thread_arg;
  tcb->prev=NULL;
  tcb->next=NULL;
  tcb->parent=NULL;
  tcb->brother=NULL;
  tcb->status=Ready;
  tcb->mode=Joinable;
  tcb->priority = priority;
  tcb->curr_priority = priority;
  tcb->tid = next_tid ++;
  tcb->timer = 0;

  //inizializzazione lista figli
  tcb->tcb_children.first = NULL;
  tcb->tcb_children.last = NULL;
  tcb->tcb_children.size = 0;
 
  //inizializzazione lista zombie
  tcb->zombie_children.first = NULL;
  tcb->zombie_children.last = NULL;
  tcb->zombie_children.size = 0;

  //stack_ptr corrisponderà all' indirizzo piu alto di un segmento restituito dal tcb_allocator   
  uint8_t *stack_ptr = (uint8_t *)stack_top;

  //scrittura indirizzo funzione trampoline nella cima della stack											   
  *stack_ptr-- = (uint8_t)((uint16_t)_trampoline & 0xFF);
  *stack_ptr-- = (uint8_t)(((uint16_t)_trampoline >> 8) & 0xFF);

  //per microcontrollori con piu memoria, ci sono indirizzi a 3 byte
#ifdef __AVR_3_BYTE_PC__											
  *stack_ptr-- = 0;
#endif


  //simula registri pushati sullo stack
  *stack_ptr-- = 0x00;    /* R2 */
  *stack_ptr-- = 0x00;    /* R3 */
  *stack_ptr-- = 0x00;    /* R4 */
  *stack_ptr-- = 0x00;    /* R5 */
  *stack_ptr-- = 0x00;    /* R6 */
  *stack_ptr-- = 0x00;    /* R7 */
  *stack_ptr-- = 0x00;    /* R8 */
  *stack_ptr-- = 0x00;    /* R9 */
  *stack_ptr-- = 0x00;    /* R10 */
  *stack_ptr-- = 0x00;    /* R11 */
  *stack_ptr-- = 0x00;    /* R12 */
  *stack_ptr-- = 0x00;    /* R13 */
  *stack_ptr-- = 0x00;    /* R14 */
  *stack_ptr-- = 0x00;    /* R15 */
  *stack_ptr-- = 0x00;    /* R16 */
  *stack_ptr-- = 0x00;    /* R17 */
  *stack_ptr-- = 0x00;    /* R28 */
  *stack_ptr-- = 0x00;    /* R29 */

  //per microcontrollori con piu memoria, altri due registri
#ifdef __AVR_3_BYTE_PC__
  *stack_ptr-- = 0x00;    /* RAMPZ */
  *stack_ptr-- = 0x00;    /* EIND */
#endif

  //scrittura dell' indirizzo dello stack aggiornato all' inizio del tcb, per utilizzo delle funzioni atomport
  tcb->sp_save_ptr = stack_ptr;	
}

/* INIZIALIZZA ALLOCATORE PER I TCB E STACK, ALLOCAZIONE CONTIGUA PER TCB E RELATIVO STACK */
PoolAllocatorResult TCB_init(){
  PoolAllocatorResult result=PoolAllocator_init(&tcb_allocator,
				  TCB_SIZE + TCB_STACK_SIZE,
				  MAX_NUM_TCB ,
				  tcb_memory_block,
				  TCB_MEMORY_BLOCK_SIZE);
  return result;
}

/* RESTITUISCE SEGMENTO PER TCB E SUO STACK */
TCB* TCB_alloc() {
  TCB* tcb = (TCB*) PoolAllocator_getBlock(&tcb_allocator);
  return tcb;
}

/* LIBERA MEMORIA PER IL TCB ED IL SUO STACK */
PoolAllocatorResult TCB_dealloc(TCB *tcb) {
   return PoolAllocator_releaseBlock(&tcb_allocator, tcb);
}
