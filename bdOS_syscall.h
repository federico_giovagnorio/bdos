#pragma once

void internal_preempt(void);
void internal_exit(void);
void internal_spawn(void);
void internal_join(void);
void internal_detach(void);
void internal_sleep(void);
void internal_write(void);
void internal_read(void);
