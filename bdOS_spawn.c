#include "bdOS_constants.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"
#include "bdOS_scheduler.h"

/* **************************** *
 *   	      SPAWN             *
 * **************************** */

extern uint8_t next_tid;
extern TCB *running;
extern TCBList *ready_queue;

/* CREA NUOVO THREAD FIGLIO DEL RUNNING E IL PADRE CONTINUA L' ESECUZIONE
 * IL NUOVO THREAD VIENE INSERITO NELLA CODA READY SE SUCCESSO, ED RITORNO È UGUALE AL TID DEL NUOVO THREAD
 * SE NON C'È MEMORIA DISPONIBILE PER IL NUOVO THREAD, ALLORA SI RITONA ERRORE */
void internal_spawn(void) {
    ThreadFn fn = running->syscall_args[0];
    void *args = running->syscall_args[1];
    uint16_t priority_arg = (uint16_t)running->syscall_args[2];
    uint16_t priority = priority_arg > 1 && priority_arg < 10  ? priority_arg : TCB_DEFAULT_PRIORITY; 

    //allocazione memoria per nuovo thread
    TCB *new_tcb = TCB_alloc();
    
    // se allocazione non avvenuta, si ritorna errore
    if (!new_tcb){
        running->syscall_retvalue = BDOS_ERR_SPAWN;
        printf("[-] No TCB aviable [internal_spawn()]\n");
        return; 
    }

    //se allocazione avvenuta, viene inizializzato il TCB ed il suo stack, quindi viene messo nella coda ready
    TCB_create(new_tcb, (Pointer)(((char *)new_tcb) + TCB_SIZE + TCB_STACK_SIZE - 1), fn, args, priority, next_tid);
    TCBList_enqueue(ready_queue, new_tcb);
    new_tcb->parent = running;
    TCBList_add_child(&running->tcb_children, new_tcb);
    running->syscall_retvalue = new_tcb->tid;

}
