#include "bdOS_tcb.h"
#include "bdOS_uart.h"

/* **************************** *
 *   	     WRITE              *
 * **************************** */

extern TCB *running;
extern UART_buffer tx_buffer;

/* SCRIVE COUNT BYTE (ARG SYSCALL) SUL BUFFER TX SE SPAZIO DISPONIBILE CON put_char
 * SCRIVE MENO BYTE DI COUNT SE C'È SPAZIO MA INCONTRA UN '\0' LUNGO IL BUFFER DA CUI LEGGE
 * SE SPAZIO NON DISPONIBILE RITORNA ERRORE, NON BLOCCANTE */
void internal_write(void){
    char *src_buffer = (char *)running->syscall_args[0];
    uint16_t num_bytes = (uint16_t)running->syscall_args[1];
    uint8_t written_bytes = 0;

    //controllo se spazio disponibile su tx_buffer
    if (num_bytes > UART_BUFFER_SIZE - tx_buffer.size){
        running->syscall_retvalue = BDOS_ERR_WRITE;
        return;
    }

    //allora c'è spazio	
    int i;
    for (i = 0; i < num_bytes; i++){
        //inserisco fino a '\0' escluso, altrimenti stampa break sull' ascoltatore di seriale
        if (src_buffer[i] == '\0')
            break;
        
        usart_putchar(src_buffer[i]);
        written_bytes++;
        
    }

    running->syscall_retvalue = written_bytes;
    return;
}
