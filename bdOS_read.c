#include "bdOS_tcb.h"
#include "bdOS_uart.h"

/* **************************** *
 *   	      READ              *
 * **************************** */

extern TCB *running;
extern UART_buffer rx_buffer;

/* LEGGE DAL BUFFER RX QUANTI PIU BYTE POSSIBILI FINO AD UN MASSIMO DI num_bytes (ARGOMENTO DELLA SYSCALL)
 * NON È BLOCCANTE ED EFFETTUA LA LETTURA TRAMITE LA get_char DELLO UART
 * RITORNA IL NUMERO DI BYTE LETTI */
void internal_read(void){
    char *dst_buffer = (char *)running->syscall_args[0];
    uint16_t num_bytes = (uint16_t)running->syscall_args[1];
    uint8_t read_bytes = 0;

    //se non ci sono caratteri da leggere nel buffer rx, si ritorna 0
    if (rx_buffer.size == 0){
        running->syscall_retvalue = 0;
        return;
    }

    //esistono dei caratteri da consumare, si continua fino a che ci sono altri caratteri oppure fino a che non si è soddisfatta la richiesta
    while (rx_buffer.size && read_bytes < num_bytes){
        char c = usart_getchar();
        dst_buffer[read_bytes] = c;
        read_bytes ++;
    }
    
    //si ritorna il numero dei byte effettivamente letti
    running->syscall_retvalue = read_bytes;
    return;
}
