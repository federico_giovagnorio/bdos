#include "bdOS_context.h"

/* **************************** *
 *     CREAZIONE CONTESTO       *
 * **************************** */

/* CREA AMBIIENTE SU CONTESTO PER ESEGUIRE UNA DATA FUNZIONE fn AL PASSAGGIO SUL CONTESTO. 
 * IL PRIMO CAMPO DI UN CONTESTO È UN POINTER, IL RESTO E LO STACK.
 * ALLORA SIMULANDO LA PRESENZA DI REGISTRI SULLO STACK DEL CONTEXT, POSSIAMO USARE LE FUNZIONI ATOMPORT COME PER I TCB */  
void bdOS_create_context(Context *context, void (*fn)(void)) {
  uint8_t *stack_ptr = (uint8_t *)(context->stack + CONTEXT_STACK_SIZE -1);

  *stack_ptr-- = (uint8_t)((uint16_t)fn & 0xFF);
  *stack_ptr-- = (uint8_t)(((uint16_t)fn >> 8) & 0xFF);

#ifdef __AVR_3_BYTE_PC__
  *stack_ptr-- = 0;												
#endif

  *stack_ptr-- = 0x00;    /* R2 */
  *stack_ptr-- = 0x00;    /* R3 */
  *stack_ptr-- = 0x00;    /* R4 */
  *stack_ptr-- = 0x00;    /* R5 */
  *stack_ptr-- = 0x00;    /* R6 */
  *stack_ptr-- = 0x00;    /* R7 */
  *stack_ptr-- = 0x00;    /* R8 */
  *stack_ptr-- = 0x00;    /* R9 */
  *stack_ptr-- = 0x00;    /* R10 */
  *stack_ptr-- = 0x00;    /* R11 */
  *stack_ptr-- = 0x00;    /* R12 */
  *stack_ptr-- = 0x00;    /* R13 */
  *stack_ptr-- = 0x00;    /* R14 */
  *stack_ptr-- = 0x00;    /* R15 */
  *stack_ptr-- = 0x00;    /* R16 */
  *stack_ptr-- = 0x00;    /* R17 */
  *stack_ptr-- = 0x00;    /* R28 */
  *stack_ptr-- = 0x00;    /* R29 */

#ifdef __AVR_3_BYTE_PC__
  *stack_ptr-- = 0x00;    /* RAMPZ */
  *stack_ptr-- = 0x00;    /* EIND */
#endif

  context->sp_save_ptr = stack_ptr;

}
