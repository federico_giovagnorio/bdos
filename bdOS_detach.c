#include "bdOS_inclib.h"

#include "bdOS_constants.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"

/* **************************** *
 *           DETACH             *
 * **************************** */

extern TCB *running;
extern TCBList *waiting_queue;

/* UN THREAD PUÒ DETACCHARE SOLO UN ALTRO THREAD CREATO DA LUI
 * SE TRA I SUOI FIGLI NON ANCORA TERMINATI, ALLORA VIENE RIMOSSO DAI SUOI CHILD E MARCATO Detached
 * SE TRA I SUOI FIGLI TERMINATI, ALLORA SI LIBERA LO ZOMBIE 
 * ALTRIMENTI, IL THREAD RICHIESTO NON È TRA I SUOI FIGLI E SI RITORNA CON UN ERRORE */
void internal_detach(void) {
    uint8_t tid_to_detach = (uint16_t)running->syscall_args[0];
    
    //controllo tra i figli
    TCB *child_to_detach = TCBList_remove_child(&running->tcb_children, tid_to_detach);
    if (child_to_detach){
        child_to_detach->parent = NULL;
        child_to_detach->mode = Detached;
        running->syscall_retvalue = tid_to_detach;
        return;
    }
    
    //controllo tra i zombie
    Zombie *zombie_to_detach = ZombieList_remove_Zombie(&running->zombie_children, tid_to_detach);
    if (zombie_to_detach) {
        Zombie_dealloc(zombie_to_detach);
        running->syscall_retvalue = tid_to_detach;
        return;
    }
    
    //non è ne tra i figli, ne tra i suoi zombie, errore
    running->syscall_retvalue = BDOS_ERR_DETACH;
    return;
}
