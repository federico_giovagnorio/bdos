#pragma once
#include "bdOS_tcb.h"

//prototype for assembly functions

/*Viene interrotto il flusso dell' old_tcb e riprende il flusso del new_tcb */
void archContextSwitch (TCB *old_tcb_ptr, TCB *new_tcb_ptr);
void archFirstThreadRestore(TCB *new_tcb_ptr);

