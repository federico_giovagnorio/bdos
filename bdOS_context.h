#pragma once 

#include "bdOS_tcb.h"

typedef struct {
    Pointer sp_save_ptr;
    char stack[CONTEXT_STACK_SIZE];
} Context;

void bdOS_create_context(Context *, void (*)(void));
