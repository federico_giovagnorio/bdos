#include "bdOS_constants.h"
#include "bdOS_tcb.h"
#include "bdOS_tcb_list.h"

/* **************************** *
 *   	      SLEEP             *
 * **************************** */

extern TCB *running;
extern TCBList *waiting_queue;
extern uint8_t need_schedule;

/* IL CHIAMANTE SARÀ MESSO IN WAITING ED IL TIMER DEL SUO TCB VERRÀ IMPOSTATO 
 * POICHE ENTRA IN WAITING SI NECESSITA SCHEDULE AL RITORNO DELLA TRAP     */
void internal_sleep(void) {
    
    //se il suo timer > 0, allora avrebbe dovuto essere ancora in waiting, errore
    if (running->timer > 0){
        running->syscall_retvalue = BDOS_ERR_SLEEP;
        return; 
    }

    //inserimento in waiting ed aggiornamento del timer, si richiede schedule
    uint16_t time_to_sleep = (uint16_t) running->syscall_args[0];
    running->timer = time_to_sleep;
    running->status = Waiting;
    TCBList_enqueue(waiting_queue, running);

    need_schedule = 1;

    return;
}
