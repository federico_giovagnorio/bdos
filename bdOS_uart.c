// ********************************************************************************
// Includes
// ********************************************************************************
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <util/delay.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "bdOS_uart.h"
// ********************************************************************************
// Macros and Defines
// ********************************************************************************
#define BAUD 19600
#define MYUBRR F_CPU/16/BAUD-1
// ********************************************************************************
// Function Prototypes
// ********************************************************************************
void usart_init(uint16_t ubrr);
char usart_getchar( void );
void usart_putchar( char data );
void usart_pstr (char *s);
unsigned char usart_kbhit(void);
int usart_putchar_printf(char var, FILE *stream);

static FILE mystdout = FDEV_SETUP_STREAM(usart_putchar_printf, NULL, _FDEV_SETUP_WRITE);

// ********************************************************************************
// usart I/O Buffers
// ********************************************************************************

UART_buffer tx_buffer;
UART_buffer rx_buffer;


// ********************************************************************************
// usart Interrupt
// ********************************************************************************

/* INSERISCE CARATTERE IN rx_buffer */
ISR(USART0_RX_vect){
    if (rx_buffer.size < UART_BUFFER_SIZE){
        char c = UDR0;
        rx_buffer.buffer[rx_buffer.end] = c;
        rx_buffer.size ++;
        rx_buffer.end ++;
        if (rx_buffer.end == UART_BUFFER_SIZE)
            rx_buffer.end = 0;
    }
}

/* PRELEVA CARATTERE DA tx_buffer E LO INVIA SULLA SERIALE */
ISR(USART0_UDRE_vect){
    if (tx_buffer.size > 0) {
        char c = tx_buffer.buffer[tx_buffer.start];
        tx_buffer.size --;
        tx_buffer.start ++;
        if (tx_buffer.start == UART_BUFFER_SIZE)
            tx_buffer.start = 0;
        UDR0 = c;
    }
    else
        UCSR0B &= ~_BV(UDRIE0);
    
}

// ********************************************************************************
// usart Related
// ********************************************************************************

/* PRELEVA CARATTERE DA rx_buffer E LO RESTITUISCE */
char usart_getchar(void) {
    
    while (rx_buffer.size==0);
    
    char c = rx_buffer.buffer[rx_buffer.start];
    rx_buffer.size --;
    rx_buffer.start ++;
    if (rx_buffer.start == UART_BUFFER_SIZE)
        rx_buffer.start = 0;

    return c;
}

/* INSERISCE CARATTERE IN tx_buffer */
void usart_putchar(char data) {
  
    while (tx_buffer.size == UART_BUFFER_SIZE);

    tx_buffer.buffer[tx_buffer.end] = data;
    tx_buffer.size ++;
    tx_buffer.end ++;

    if (tx_buffer.end == UART_BUFFER_SIZE)
        tx_buffer.end = 0;

    UCSR0B |= _BV(UDRIE0);
}

void usart_init( uint16_t ubrr) {
    // Set baud rate
    UBRR0H = (uint8_t)(ubrr>>8);
    UBRR0L = (uint8_t)ubrr;

    UCSR0C = _BV(UCSZ01) | _BV(UCSZ00); /* 8-bit data */ 
    UCSR0B = _BV(RXEN0) | _BV(TXEN0) |  _BV(RXCIE0);   /* Enable RX and TX */

    tx_buffer.start = 0;
    tx_buffer.end = 0;
    tx_buffer.size = 0;

    rx_buffer.start = 0;
    rx_buffer.end = 0;
    rx_buffer.size = 0;

    sei();
}

void usart_pstr(char *s) {
    // loop through entire string
    while (*s) { 
        usart_putchar(*s);
        s++;
    }
}
 
// this function is called by printf as a stream handler
int usart_putchar_printf(char var, FILE *stream) {
    // translate \n to \r for br@y++ terminal
    if (var == '\n') usart_putchar('\r');
    usart_putchar(var);
    return 0;
}

void printf_init(void){
    stdout = &mystdout;
  
    // fire up the usart
    usart_init ( MYUBRR );
}

